<?php
    /** -------------------------------------------------------------------------- **
     *  Index Page for Resume                                                       *
     * --------------------------------------------------------------------------- **
     *  File        index.php                                                       *
     *  Author      Reinhardt Zundorf                                               *
     *  Created     26/02/2017      Updated     01/03/2017                          *
     ** -------------------------------------------------------------------------- **/

    /** -------------------------------------------------------------------------- **
     *  Load 'config.php'                                                           *
     ** -------------------------------------------------------------------------- **/
    require 'config.php';
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <!-- Page title -->
        <title><?php echo $pageTitle; ?></title>

        <!-- Meta tags -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"              content="width=device-width, initial-scale=1.0">
        <meta name="description"           content="Reinhard Zundorf | Resume & Curriculum Vitae">
        <meta name="author"                content="Reinhardt Zundorf">

        <!-- Stylesheets -->
        <link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/styles.css">

        <!-- Favico -->
        <link rel="shortcut icon" href="favicon.ico" type="text/css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head><!-- /head -->

    <!-- Content -->
    <body>

        <!-- Page wrapper -->
        <div class="wrapper">

            <!-- Sidebar -->
            <div class="sidebar-wrapper">

                <!-- Applicant details -->
                <div class="profile-container">

                    <!-- Profile -->
                    <img class="img-circle profile" src="img/cv-profile-pic.jpg" alt="Reinhardt Zundorf Curriculum Vitae"/>

                    <!-- Name & surname -->
                    <h1 class="name">Reinhardt Zundorf</h1>

                    <!-- Job title -->
                    <h3 class="tagline">Developer</h3>

                </div><!-- /profile -->

                <!--                 CV download options 
                <div class="container-block">

                     Heading 
                    <h2 class="container-block-title">Curriculum Vitae</h2>

                     Download options 
                    <ul class="list-inline downloads-list">

                         Download: PDF
                        <li>
                            <a href="./download/r_zundorf-cv-2017.pdf" target="_blank">
                                <img class="cv-icon" src="img/cv-icon-pdf.png" alt="Download CV in PDF format"/>
                            </a>
                        </li> /download_pdf 

                         Download: Word 
                        <li>
                            <a href="./download/r_zundorf-cv-2017.docx" target="_blank">
                                <img class="cv-icon" src="img/cv-icon-word.png" alt="Download CV in Word format"/>
                            </a>
                        </li> /download_word 

                         Print 
                        <li>
                            <a href="./download/r_zundorf-cv-2017.odt" target="_blank">
                                <img class="cv-icon" src="img/cv-icon-print.png" alt="Printable Version"/>
                            </a>
                        </li> /download_resume_pdf 
                    </ul> /download_options 
                </div> /download -->

                <!-- Contact details -->
                <div class="contact-container container-block">

                    <!-- Heading -->
                    <h2 class="container-block-title">Contact Details</h2>

                    <!-- Contact information list -->
                    <ul class="list-unstyled contact-list">
                        <!-- Email address(es) -->
                        <li class="email">
                            <div class="row">
                                <div class="col-md-1"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                                <div class="col-md-10"><a href="mailto:reinhardt.zundorf@gmail.com">reinhardt.zundorf@gmail.com</a></div>
                            </div>
                        </li><!-- /email_address -->

                        <!-- Phone (H) -->
                        <li class="phone">
                            <div class="row">
                                <div class="col-md-1"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                <div class="col-md-10"><a href="tel:0217851406">(+27) 021 785 1406</a></div>
                            </div>
                        </li><!-- /phone -->

                        <!-- Mobile -->
                        <li class="phone">
                            <div class="row">
                                <div class="col-md-1"><i class="fa fa-mobile-phone" aria-hidden="true"></i></div>
                                <div class="col-md-10"><a href="tel:0612955132">(+27) 061 295 5132</a></div>
                            </div>
                        </li><!-- /mobile -->

                        <!-- Skype -->
                        <li class="phone">
                            <div class="row">
                                <div class="col-md-1"><i class="fa fa-skype" aria-hidden="true"></i></div>
                                <div class="col-md-10"><a href="skype:reinhardt.zundorf1?call" target="_blank">reinhardt.zundorf1</a></div>
                            </div>
                        </li><!-- /skype -->

                        <!-- LinkedIn -->
                        <li class="linkedin">
                            <div class="row">
                                <div class="col-md-1"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
                                <div class="col-md-10"><a href="https://www.linkedin.com/in/reinhardt-zundorf" target="_blank">linkedin.com/in/reinhardt-zundorf</a></div>
                            </div>
                        </li><!-- /linkedin -->
                    </ul>
                </div>

                <!-- Qualifications -->
                <div class="education-container container-block">

                    <!-- Title -->
                    <h2 class="container-block-title">Education</h2>

                    <!-- Qualifications -->
                    <?php
                        /** ---------------------------------------------------------------- *
                         *  Loop through container and output the attributes for each qual-  *
                         *  ification object.                                                *
                         * ---------------------------------------------------------------- **/
                        foreach ($eduArray as $eduObject) 
                        {                        
                            $eduObject->outputAttributes();
                        }
                    ?>
                </div>

                <!-- LANGUAGES -->
                <div class="languages-container container-block">
                    <h2 class="container-block-title">Languages</h2>
                    <ul class="list-unstyled languages-list">
                        <?php
                            /** ---------------------------------------------------------------------------- *
                             * Loop through the 'lang' container & print the html for each of the languages  *
                             * ---------------------------------------------------------------------------- **/
                            foreach ($lang as $key => $value) 
                            {
                            	$temp = $lang[$key]+"%";

                            	echo "<li>";
                            	echo   "<div class='row'>";
                            	echo       "<div class='col-md-6'>$key</div>";
                            	echo       "<div class='col-md-6'><span class='lang-desc'>($value)</span></div>";
                            	echo   "</div>";
                            	echo "</li>";
                            }
                        ?>
                    </ul>
                </div>

                <!-- INTERESTS/HOBBIES -->
                <div class="interests-container container-block">
                    <h2 class="container-block-title">Interests</h2>
                    
                    <!-- interests list -->
                    <ul class="list-unstyled interests-list">
                        <?php
                            /* ------------------------------------------------------------- *
                             * Loop through the '$hobbiesArray' printing out key-value pairs. *
                             * ------------------------------------------------------------- */
                            foreach ($hobbiesArray as $key => $value) 
                            {
                                /* ----------------------------------------------------- *
                                 * Output list item as HTML                              *
                                 * ---------------------------------------------------- **/
                                /*  <ul class="list-unstyled interests-list">   */
                                echo    "<li>$value</li>";                               
                            /*  ... </ul>       */
                            }
                        ?>
                    </ul><!-- /interests-list -->
                </div><!--/interest-div-->
            </div><!--/sidebar-wrapper-->

            <!-- Main wrapper -->
            <div class="main-wrapper">

                <!-- Career summary section -->
                <section class="section summary-section">

                    <!-- Heading -->
                    <h2 class="section-title">
                        <i class="fa fa-user"></i>
                        Career Profile
                    </h2><!-- /heading -->

                    <!-- Content -->
                    <div class="summary">
                        <!-- Para. 1 -->
                        <p>
                            I am a well-rounded 24-year-old male, with a Higher Certificate in Information Systems: Software Development
                            and a Certificate in Java Programming. In addition to this, I am currently studying towards
                            becoming a Java SE 7 Certified Programmer through Oracle University.
                        </p><!-- para_1 -->

                        <!-- Para. 2 -->
                        <p>
                            Previously I was employed as a Junior Programmer at an IT firm specialising in the delivery of
                            financial services and solutions. I dramatically increased their productivity, by contributing to a
                            measurable increase in efficient C code and new functionality for their Point of Sale devices.
                            I would like to repeat this success at your company. I have outstanding communication and interpersonal skills.
                            I thrive on challenges. My excellent problem-solving skills are testimonial to my job success.
                        </p><!-- para_2 -->

                        <!-- Para. 3 -->
                        <p>
                            I enjoy working with a diverse group of people and have a passion for programming and development.
                            The range of skills I have acquired from my work experience and academic background give support
                            to my genuine interest in programming, software, Object Oriented design and financial systems.
                        </p><!-- para_3 -->
                    </div><!-- /summary-->
                </section><!-- /summary_sect -->

                <!-- Working experience -->
                <section class="section experiences-section"
                    <!-- Heading -->
                    <h2 class="section-title">
                        <i class="fa fa-briefcase"></i>
                        Working Experience
                    </h2>

                    <!-- Working Entry I -->
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">

                                <!-- Entry title -->
                                <h3 class="job-title">
                                    <?php echo $positionJob1; ?>
                                </h3><!-- /title -->

                                <!-- Time/duration -->
                                <div class="time">
                                    <?php echo $positionTime1; ?>
                                </div><!-- /time -->
                            </div><!-- /row -->

                            <!-- Name of company -->
                            <div class="company">
                                <?php echo $positionCompany1; ?>
                            </div><!-- /name -->
                        </div><!-- /company -->

                        <!-- Details -->
                        <div class="details">
                           <p><?php echo $positionDetails1; ?></p>
                        </div><!-- /details -->
                    </div><!-- /entry -->

                    <!-- Working Entry II -->
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">

                                <!-- Job title -->
                                <h3 class="job-title">
                                    <?php echo $positionJob2; ?>
                                </h3><!-- title -->

                                <!-- Time/duration -->
                                <div class="time">
                                    <?php echo $positionTime2; ?>
                                </div><!-- /time -->
                            </div><!-- /row -->

                            <!-- Company name -->
                            <div class="company">
                                <?php echo $positionCompany2; ?>
                            </div><!-- /details -->
                        </div><!-- /meta -->

                        <!-- Job description -->
                        <div class="details">
                            <?php echo $positionDetails2; ?>
                        </div><!-- /desc. -->
                    </div><!-- /entry -->

                    <!-- JOB III -->
                    <div class="item">
                        <div class="meta">
                            <div class="upper-row">
                                <h3 class="job-title">
                                    <?php echo $positionJob3; ?>
                                </h3>
                                <div class="time">
                                    <?php echo $positionTime3; ?>
                                </div>
                            </div>
                            <div class="company">
                                <?php echo $positionCompany3; ?>
                            </div>
                        </div>
                        <div class="details">
                            <?php echo $positionDetails3; ?>
                        </div>
                    </div>
                </section>

                <!-- SKILLS MATRIX -->
                <section class="skills-section section">
                    <h2 class="section-title">
                        <i class="fa fa-rocket"></i>
                        Skills Matrix
                    </h2>

                    <div class="skillset">
                    <?php
                        /** ----------------------------------------------------------------------------------- **
                         *  Loop through the '$skillsArray' & output key-value ('skill', 'level').               *
                         * ------------------------------------------------------------------------------------ **/
                        foreach ($skillsArray as $key => $value) {
                        	echo "<div class='item'>";
                        	echo "<h3 class='level-title'>$key</h3>";
                        	echo "<div class='level-bar'>";
                        	echo "<div class='level-bar-inner' data-level='$temp%'></div>";
                        	echo "</div>";
                        	echo "</div>";
                        }
                    ?>
                    </div>
                </section>

                <!-- Own projects & part-time work -->
                <section class="section projects-section">

                    <!-- Heading -->
                    <h2 class="section-title"><i class="fa fa-archive"></i>Projects</h2><!-- /heading -->

                    <!-- PHP: output each entry -->
                    <?php
                        /** ---------------------------------------------------------------------- **
                         * Loop through the '$projectsArray' printing out key-value pairs.          *
                         * ----------------------------------------------------------------------- **/
                        foreach ($projectsArray as $key => $value) 
                        {
                        	$temp = $projectsArray[$key]+"%";

                        	echo "<div class='item'>";
                        	echo   "<span class='project-title'>$key</span>";
                        	echo   "<div class='details'>$value</div>";
                        	echo "</div>";
                        }
                    ?><!-- /projectsArray -->

                </section><!--/section -->
            </div><!-- /main -->
        </div><!-- /wrapper -->

        <!-- Footer -->
        <footer class="footer">
            <div class="text-center">Copyright &copy; Reinhardt Zundorf 2017</div>
        </footer><!-- /footer -->

        <!-- JavaScript imports -->
        <script type="text/javascript" src="plugins/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="js/main.js"></script><!-- /scripts -->
    </body><!-- /body -->
</html><!-- /html -->