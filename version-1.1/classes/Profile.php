<?php

    /** ------------------------------------------------------------------------**
     * 'Profile' PHP Class                                                       *
     * ------------------------------------------------------------------------- *
     *   Name         ./classes/Profile.php                                      *
     *   Date         02/03/2017                                                 *
     *   Author       Reinhardt Zundorf                                          *
     *   Desc                                                                    *
     ** ----------------------------------------------------------------------- **/
    class Profile 
    {
        /** ------------------------------------------------------------------------ *
         *  Variables (attributes)                                                   *
         * ------------------------------------------------------------------------ **/
        private $name;
        private $surname; 
        private $title;
        private $email;
        private $homeTel;
        private $mobile;
        private $skype;
        private $linkedin;
        private $profilePic;            /* stores profile picture URL on the server */
                
        /** ------------------------------------------------------------------------ *
         *  Constructor                                                              *
         * ------------------------------------------------------------------------ **/
        public function __construct($name, $surname, $title, $email, $homeTel, $mobile,$skype, $linkedin, $profilePic) 
        {
            $this->name         = $name;
            $this->surname      = $surname;
            $this->title        = $title;
            $this->email        = $email;
            $this->homeTel      = $homeTel;
            $this->mobile       = $mobile;
            $this->skype        = $skype;
            $this->linkedin     = $linkedin;
            $this->profilePic   = $profilePic;
    }
        
        /** ------------------------------------------------------------------------ */
        /*  Accessor Methods                                                         */
        /* ------------------------------------------------------------------------ **/
        public function getName() 
        {
            return $this->name;
        }

        public function getSurname()
        {
            return $this->surname;
        }

        public function getJobTitle() 
                {
            return $this->title;
        }

        public function getEmail() {
            return $this->email;
        }

        public function getHomeTel() {
            return $this->$homeTel;
        }

        public function getPhoneMobile() {
            return $this->mobile;
        }

        public function getSkype() 
        {
            return $this->skype;
        }

        public function getLinkedin() 
        {
            return $this->linkedin;
        }

        
        
        /** ----------------------------------------------------------------------- **
         *  Mutator Methods                                                          *
         ** ----------------------------------------------------------------------- **/
        public function setName($name) 
        {
            $this->name = $name;
        }

        public function setSurname($surname) 
        {
            $this->surname = $surname;
        }

        public function setJobTitle($title) 
        {
            $this->title = $title;
        }

        public function setEmail($email) 
        {
            $this->email = $email;
        }

        public function setPhoneHome($homeTel) 
        {
            $this->phoneHome = $homeTel;
        }

        public function setPhoneMobile($mobile) 
        {
            $this->mobile = $mobile;
        }

        public function setSkype($skype) 
        {
            $this->skype = $skype;
        }

        public function setLinkedin($linkedin) 
        {
            $this->linkedin = $linkedin;
        }
        
        public function setProfileURL($profilePic)
        {
            $this->$profilePic = $profilePic;
        }
        
    }      
?>