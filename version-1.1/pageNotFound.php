<?php
    /** -------------------------------------------------------------------------- **
     *  404 Page Not Found Error Page for 'dynam-resume'                            *
     * --------------------------------------------------------------------------- **
     *  File        pageNotFound.php                                                *
     *  Author      Reinhardt Zundorf                                               *
     *  Created     27/02/2017      Updated     27/02/2017                          *
     ** -------------------------------------------------------------------------- **/
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <!-- Title -->
        <title><?php echo $pageTitle; ?></title>

        <!-- Meta -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"              content="width=device-width, initial-scale=1.0">
        <meta name="description"           content="Reinhard Zundorf | Resume & Curriculum Vitae">
        <meta name="author"                content="Reinhardt Zundorf">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/styles.css">

        <!-- Favico -->
        <link rel="shortcut icon" href="favicon.ico" type="text/css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head><!-- /head -->

    <!-- Content -->
    <body>

       <!-- Page wrapper -->
        <div class="wrapper">

            <!-- first section -->
            <div class="container-fluid" style="color:blue; height:200px;">
                <div class="row">
                    
                    !-- display the error code to the user -->
                    <h1>404</h1>
                </div><!-- /container_fluid -->
            </div><!-- /section_1 -->

            <!-- second section -->
            <div class="container-fluid" style="color:white;">
                
                <!-- more detailed message & email link -->
                <h2>
                    Page could not be found. Please contact 
                    <a href="mailto:reinhardt.zundorf@gmail.com" class="email_address">me</a>.
                </h2><!-- /h2 -->
            </div><!-- /section_2 -->
        </div><!-- /wrapper -->

        <!-- Footer -->
        <footer class="footer">
            <div class="text-center">Copyright &copy; Reinhardt Zundorf 2017</div>
        </footer><!-- /footer -->

        <!-- JavaScript imports -->
        <script type="text/javascript" src="plugins/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="plugins/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="js/main.js"></script><!-- /scripts -->
    </body><!-- /body -->
</html><!-- /html -->