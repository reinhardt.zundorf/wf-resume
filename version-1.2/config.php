<?php

/** ------------------------------------------------------------------------------ **
 * Configuration for 'dynamic-cv'                                                   *
 * ------------------------------------------------------------------------------- **
 * Filename     cvConfig.php                                                        *
 * Author       Reinhardt Zundorf                                                   *
 * Created      24/01/2017                                                          *
 * Updated      27/01/2017                                                          *
 * * ------------------------------------------------------------------------------ **

 * * ------------------------------------------------------------------------------ **
 *  logging                                                                         *
 * * ------------------------------------------------------------------------------ **
 *  Sample: [2015-08-14 09:08:23] [/var/www/html/index.php] [warning] "Message."    *
 *  Format: [Y-m-d h:m:s]         [/dir/dir/dir/file.ext]   [type]    "alosmorola"  *
 * * ------------------------------------------------------------------------------ * */
include "classes/Education.php";

/** ------------------------------------------------------------------------------ **
 *  Set logging variables                                                               *
 * * ------------------------------------------------------------------------------ * */
$date  = date("Y-m-d h:m:s");
$file  = __FILE__;
$level = "INFO";


/* -------------------------------------------------------------------------------- */

$log_msg = "";

/** ------------------------------------------------------------------------------ **
 *  Include 'Education' class & log                                                 *
 * * ------------------------------------------------------------------------------ * */
$message = "{$date} {$file} {$level} : Logging configured & classes included. Start of config file." . PHP_EOL;

error_log($message);

/* ---------------------------------------------------------------------------- *
 * General / Page
 * ---------------------------------------------------------------------------- */
$pageTitle = "Curriculum Vitae | Reinhardt Zŭndorf";

/** ------------------------------------------------------------------------- **
 * Create instances of the 'Education' class for every qualification that the
 * user wants to add. 'Education' objects get added to an array, which is 
 * iterated through in the 'index.php' file. 
 * -------------------------------------------------------------------------- * */
$log_msg = "Populating 'Education' objects.";
$message = "[{$date}] [{$file}] [{$level}] [$log_msg]]" . PHP_EOL;

error_log(message);

/* ---------------------------------------------------------------------------- */

$eduObject1 = new Education("Higher Certificate in Information Systems", "CTI Education", "2013 - 2013");
$eduObject2 = new Education("Certificate in Advanced C++ Programming", "CTI Education", "2013 - 2014");
$eduObject3 = new Education("Certificate in Java Programming", "CTI Education", "2013 - 2014");

/** -------------------------------------------------------------------------- **
 *  Insert Education objects into a key-value array                             *
 * * -------------------------------------------------------------------------- * */
$eduArray = array($eduObject1, $eduObject2, $eduObject3);

/* ---------------------------------------------------------------------------- *
 * Career / Work Experience
 * ---------------------------------------------------------------------------- */
$positionJob1     = "Developer";
$positionCompany1 = "Grapevine Interactive";
$positionTime1    = "March 2016 - December 2016";
$positionDetails1 = "Development to specification of marketing campaigns
                             predominantly on USSD & push message channels. USSD promotions (or 
                             'campaigns') on Grapevine's platform are exposed as Java Web Services 
                             and require comprehensive knowledge of Java EE, classes & interface, 
                             XML files, PostgreSQL and Ubuntu Linux.";

/* ----------------------------------------------------------------------------- */

$positionJob2     = "Programmer";
$positionCompany2 = "Tacheyon Technologies";
$positionTime2    = "April 2015 - August 2015";
$positionDetails2 = "Contributing to and maintaining the Point of Sale device’s 
                    software in C. Testing of the company’s banking software suite: 
                    production of bank cards and KYC. Coding in C on Stratus Computers OpenVOS 
                    operating system (backend).";

/* ----------------------------------------------------------------------------- */

$positionJob3     = "Software Developer";
$positionCompany3 = "TSS Software Solutions";
$positionTime3    = "March 2014 - September 2014";
$positionDetails3 = "Design, development and implementation of new on a GPS tracking software product.
                    Completion of tasks assigned; from conceptualisation to implementation.
                    Documentation, bug and error testing of code. Applying modern standards 
                    and conventions to pre-existing Java code. Writing of new Java Web Applications - XML, HTML,
                    JavaScript & Java SE 6.";


/* ---------------------------------------------------------------------------------------------- *
 * Skills Matrix
 * ---------------------------------------------------------------------------------------------- */
$log_msg = "Populate projects section.";
$message = "[{$date}] [{$file}] [{$level}] [$log_msg]]" . PHP_EOL;

$projectsArray = array("Insight Student Volunteers"
    => "Website & Online Portal. Application Management System built in PHP."
    . "The portal allows the company to log in and view new applicants, rate "
    . "and approve them as volunteers.");


/* ---------------------------------------------------------------------------------------------- */

reset($projectsArray);

/* ---------------------------------------------------------------------------------------------- */

$skillsArray = array("Java Standard Edition"           => 70,
    "Java Enterprise Eidition"        => 70,
    "Struts"                          => 60,
    "C (Point of Sale Devices"        => 60,
    "Android Application Development" => 50,
    "C++ (STL)"                       => 50,
    "Web Development"                 => 80,
    "PHP (back-end & front-end)"      => 70,
    "MySQL & PostgreSQL Databases"    => 80,
    "Oracle Database 11 & 12c"        => 50,
    "Planning & Design (UML & SDLC)"  => 80,
    "Perl & Bash Scripting"           => 60);

/* ---------------------------------------------------------------------------- */

reset($skillsArray);

/* ---------------------------------------------------------------------------- *
 * Environments, Tools & IDEs
 * ---------------------------------------------------------------------------- */
$toolsArray = array("Microsoft Windows (7 - 10)",
    "Ubuntu Linux (12.xx - 16.10)",
    "Apple OS X (Mavericks)",
    "Solus Linux (1.xx)",
    "Eclipse IDE (Java EE version)",
    "IngeDev IDE (Ingenico's version)",
    "Idea WebStorm IDE",
    "Idea Intellij IDE",
    "NetBeans IDE (5.0 - 8.2)");

/* ---------------------------------------------------------------------------- */

reset($toolsArray);

/* --------------------------------------------------------------------------- **
 * Languages                                                                    *
 * --------------------------------------------------------------------------- * */
$lang = array("English"   => "Fluent",
    "Afrikaans" => "Fluent",
    "German"    => "Elementary");

/* ---------------------------------------------------------------------------- */

reset($lang);

/* --------------------------------------------------------------------------- **
 * Hobbies & Other Work                                                         *
 * --------------------------------------------------------------------------- * */
$hobbiesArray = array("Reading",
    "Baroque Music",
    "Piano",
    "New Tech",
    "Web Development");

/* ---------------------------------------------------------------------------- */

reset($hobbiesArray);

/* ---------------------------------------------------------------------------- */
?>
