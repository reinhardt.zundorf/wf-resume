<?php

/** ------------------------------------------------------------------------ *
 * 'Education' PHP Class                                                     *
 * ------------------------------------------------------------------------- *
 *   Name         ./classes/Education.php                                    *
 *   Date         01/03/2017                                                 *
 *   Author       Reinhardt Zundorf                                          *
 *   Desc         n/a                                                        *
 *   Updated      02/03/2017                                                 *
 * ------------------------------------------------------------------------ **/
class Education 
{
    /** ------------------------------------------------------------------------ *
     *  Variables (attributes)                                                   *
     * ------------------------------------------------------------------------ **/
    private $nameOfQual;     
    private $placeOfStudy;
    private $studyTime;

    /** ------------------------------------------------------------------------ *
     *  Constructor                                                              *
     * ------------------------------------------------------------------------ **/
    public function __construct($nameOfQual, $placeOfStudy, $studyTime) 
    {
        $this->nameOfQual   = $nameOfQual;
        $this->placeOfStudy = $placeOfStudy;
        $this->studyTime    = $studyTime;
    }

    
    
    /** ------------------------------------------------------------------------ *
     *  outputAttributes()                                                       *
     * ------------------------------------------------------------------------- *
     *  DESC.     outputs the values of '$nameQual', '$placeOfStudy' and         *
     *            '$studyTime' as a list item.                                   *
     *  UPDATED   02/03/2017                                                     *
     * ------------------------------------------------------------------------ **/
    public function outputAttributes()
    {
        /** ------------------------------------------------------------------------ *
         *  Check that the variables aren't blank                                    *
         * ------------------------------------------------------------------------ **/
        if(isset($this->nameOfQual) && isset($this->placeOfStudy) && isset($this->studyTime))
        {
            /** ------------------------------------------------------------------------ *
             *  Build the '$output' string containing the HTML code for a 'Education'    *
             *  is_object(var)                                                           *
             * ------------------------------------------------------------------------ **/
            $output = "<div class='item'>"
                        . "<h4 class='degree'>" . $this->nameOfQual    . "</h4>"
                        . "<h5 class='meta'>"   . $this->placeOfStudy  . "</h5>"
                        . "<div class='time'>"  . $this->studyTime     . "</div>"
                    . "</div>";

            echo $output;
        }
        else
        {
            /* -------------------------------------------------- **
             * All  variables are blank - do nothing               * 
             * -------------------------------------------------- **/
        }
    }
    
    /** ------------------------------------------------------------------------ */
    /*  Accessor Methods                                                         */
    /* ------------------------------------------------------------------------ **/
    public function getNameOfQual() 
    {
        return $this->nameOfQual;
    }

    public function getPlaceOfStudy() 
    {
        return $this->placeOfStudy;
    }

    public function getStudyTime() 
    {
        return $this->studyTime;
    }


    /** ------------------------------------------------------------------------ */
    /*  Mutator Methods                                                         */
    /* ------------------------------------------------------------------------ **/
    public function setNameOfQual($nameOfQual) 
    {
        $this->nameOfQual = $nameOfQual;
    }

    public function setPlaceOfStudy($placeOfStudy) 
    {
        $this->placeOfStudy = $placeOfStudy;
    }

    public function setStudyTime($studyTime) 
    {
        $this->studyTime = $studyTime;
    }
}
?>